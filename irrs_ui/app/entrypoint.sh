#!/bin/bash
/etc/init.d/mysql start && \
    mysql -u root -proot -e "CREATE DATABASE IF NOT EXISTS irrs_gui_dev;"

# mysql -u root -proot -e $'USE irrs_gui_dev;INSERT INTO config(dataset,api,columns,parameter) VALUES ("irrs_qa","http://<>/find_ticket",\'[{"title": "Incident Name","data": "name"},{"title": "URL","data": "url"},{"title": "Score","data": "score","sType" : "numeric"}]\',\'{"feedback" : "1","table" : "1","url":"1","summary" : "0","embedding" :"0" }\')'

# mysql -u root -proot -e $'USE irrs_gui_dev;INSERT INTO config(dataset,api,columns,parameter) VALUES ("irrs_prod","http://<>/find_ticket",\'[{"title": "Incident Name","data": "name"},{"title": "URL","data": "url"},{"title": "Score","data": "score","sType" : "numeric"}]\',\'{"feedback" : "0","table" : "1","url":"0","summary" : "0","embedding" :"0" }\');'

# mysql -u root -proot -e $'USE irrs_gui_dev; CREATE TABLE IF NOT EXISTS token(id bigint unsigned NOT NULL AUTO_INCREMENT, username VARCHAR(100), endpoints SET("GetSimilarTickets"), domain VARCHAR(20), expiry DATE, token VARCHAR(255),time timestamp NOT NULL default current_timestamp, primary key (id));'
# mysql -u root -proot -e $'USE irrs_gui_dev;INSERT INTO token(username, endpoints, token) VALUES ("admin","GetSimilarTickets","test_token");'

service redis-server restart
npm start
